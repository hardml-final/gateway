import os
import pickle

import numpy as np
import requests
from flask import Flask, Response, jsonify, request
from sentence_transformers import SentenceTransformer


def load_clusters_centres(path: str):
    """
    Загрузить файл с центрами кластеров
    :param path: str, путь до файла
    :return: np.ndarray, центры кластеров
    """
    with open(path, "rb") as file:
        res = pickle.load(file)

    return np.array([val for _, val in res.items()])


centres = load_clusters_centres(os.environ["ROOT_FOLDER"] + "/" + os.environ["CLUSTER_CENTRE_PATH"])

app = Flask(__name__)

model = None


@app.before_first_request
def load_model():
    """
    Загрузка модели
    Только таким образом работает с uWSGI
    :return: None
    """
    global model
    model = SentenceTransformer(os.environ["MODEL_NAME"])


def embed(_input):
    """
    Получить эмбеддинг для фразы
    :param _input: List[str], список из одной строки-запроса
    :return: np.ndarray, 384-мерный эмбеддинг
    """
    return model.encode(_input)


@app.route('/ping', methods=["GET"])
def ping():
    """
    Healthcheck
    :return: {"status": "ok"} if the service is ready
    """
    return jsonify(status="ok")


@app.route('/predict', methods=["POST"])
def predict():
    """
    Получить дубликаты к запросу
    :return: json с дубликатами по мнению сервиса
    """
    # получаем эмбеддинг запроса с помощью модели
    query = request.get_json()["query"]
    emb = embed(query)

    # находим ближайшую центроиду кластер
    cluster = np.argmin(np.linalg.norm(emb - centres, axis=1))

    # отправляем запрос в индекс на поиск похожих
    response = requests.post(os.environ["INDEXER_ENDPOINT"].format(i=cluster),
                             json={"embedding": emb.tolist()})

    # возвращаем ответ
    if response.status_code == 200:
        return jsonify(status="ok", relevant_docs=response.json()["relevant_docs"])
    else:
        return Response("Something went wrong while searching in index for relevant docs", status=501)
