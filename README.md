Основная информация в [update_index](https://gitlab.com/hardml-final/update_index).

# Gateway

Basic archtecture:
![architecture](architecture.png)
This repo is for `API gw` and `эмбеддер` parts.

Gateway for similar text search. Emeds input text, finds the closest cluster centre for the query, and redirects it to the corresponding server.  

[Sber's transformer](https://huggingface.co/sentence-transformers/all-MiniLM-L6-v2) used for embedding of model. 

Cluster centres precalculated in advance and loaded from disk.   

Service where the query is being redirected described [here](https://gitlab.com/hardml-final/embedder).

## Endpoints

- `/predict` – main endpoint, get embedding of query and return top 10 most similar objects in index;
- `/ping` – healthcheck, returns 200 json `{"status": "ok"}`.
