FROM python:3.9

WORKDIR /app
COPY . /app

RUN pip install torch==1.13.0 && pip install -r requirements.txt

# скачиваем модель при сборке образа, чтобы повторно не скачивать при развёртке приложения
ARG model_name=paraphrase-MiniLM-L6-v2
RUN python -c "from sentence_transformers import SentenceTransformer; _ = SentenceTransformer('${model_name}')"

CMD ["uwsgi", "--http", "0.0.0.0:80", "--master", "-p", "1", "--threads", "1", "-w", "gateway:app"]
